var correctAnswers = ['A','B','B','A','A'];
var form = document.querySelector('.quiz-form');
var result = document.querySelector('.result');

form.addEventListener('submit', e => {
    e.preventDefault()


let score = 0;
var userAnswers = [form.q1.value, form.q2.value, form.q3.value, form.q4.value, form.q5.value];

userAnswers.forEach((answers, index) =>{
   if( answers == correctAnswers[index]){
       score += 20;
   }
});
scrollTo(0,0);
result.classList.remove('d-none');

let calculate = 0;
const timer = setInterval(() => {
    result.querySelector('span').textContent = `${calculate}%`;
    if(calculate == score){
        clearInterval(timer);
    }
    else{
        calculate++;
    }
}, 100);

// let calculate = 0;
// const timer = setInterval(() => {
// result.querySelector('span').textContent = `${calculate}%`;
// if(calculate == score){
//     clearInterval(timer);
// }
// }, 20);
});